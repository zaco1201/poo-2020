public abstract class Aparato implements Interruptor{
    private static  int contadorAparatos = 0;

    private String nombre;
    private String marca;
    private String estado = false;
    private int id;

    public Aparato(String nombre, String marca){
        super();
        this.nombre = nombre;
        this.marca = marca;
        this.estado = false;
        this.id = contadorAparatos + 1;
    }

    public string toString(){
        String texto = id + " " + nombre + " " + marca;
        if (estado){
            texto = texto + "[On]";
        }
        else{
            texto = texto + "[Off]";
        }
        return texto;
    }

    public void encender(){
        this.estado = true;
    }

    public void apagar(){
        this.estado = false;
    }
}