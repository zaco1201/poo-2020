public class Main {
    public static void main(String[] args){
        AparatoTermoRegulable atc = new AparatoTermoRegulable("Aire Acondicionado", "LG", 10, 35);
        atc.encender();
        atc.apagar();

        float[] canales = new float[] {7f, 102f, 150f, 480f};
        AparatoConSeñal acs = new AparatoConSeñal("Televisor", "SAMSUNG", canales);
        acs.encender();
        acs.apagar();

        String[] modalidades = {"delicado", "intermedio", "fuerte"};
        AparatoConModalidad acm = new AparatoConModalidad("Lavadora", "Whirpool", modalidades);
        acm.encender();
        acm.apagar();
    }
}